
var _toggleButton
    ,   url = undefined
    ,   _domain = undefined
    ,   _credentialsList = ''
    ,   _credentialsobj = ''
    ,   _savedCredentials = []
    ,   _environment = ''
    ,   _staticEnvironment = '' 
    ,   _listCredentials = ''
    ,   _domainValue = ''
    ,   _domainIndex = ''
    ,   _usernameValue = ''
    ,   _usernameIndex = ''
    ,   _passwordObject = ''
    ,   _env = ''
    ,   name = ''
    ,   username = ''
    ,   password = ''
    ,   validateState = false
    ,   storePayload = ''
    ,   _findIndex = ''
    ,   _createEnv = ''
    ,   _envList = ''
    ,   _messageObject = {}
    ,   _noDataText = '';

document.body.onload = function(){
    $("#version").text(browser.runtime.getManifest().version);
    browser.storage.sync.get('data', function(items){
        if(items != undefined && items.data != undefined && items.data.length !== 0){
            _savedCredentials = items.data;
        }else{
            _savedCredentials = [];   
        }
        init();
    });
}

function init(){
    getDomainName();
            
    $('#switchBtn').on('click', function(){
        if($('#storeCS').is(':visible')){
            $('#storeCS').hide();
            $('#credentialsList, #listBtn, #addBtn').show();
            $(this).addClass('rotateToggle').attr("title", "About Credentials Store");
        }
        else if($('#credentialsList').is(':visible')) {
            $('#credentialsList, #listBtn, #addBtn').hide();
            $('#aboutCS, #resetBtn').show();
            $(this).removeClass('rotateToggle').attr("title", "Go back");
        } else {
            $('#aboutCS, #resetBtn').hide();
            $('#credentialsList, #listBtn, #addBtn').show();
            $(this).addClass('rotateToggle').attr("title", "About Credentials Store");
        }
        if(_savedCredentials.length == 0){
            setNoData();
        }
    });

    $('#resetBtn').on('click',function(){
        if(_savedCredentials.length > 0){
            browser.storage.sync.set({'data' : []});
            $(this).addClass('rotateToggleFull');
            setTimeout(function(){location.reload();},500);
        }
    });

    $('#listBtn').on('click', function(){
        if($(this).hasClass('rotateToggle') && _credentialsobj != undefined){
            buildCredentials();
            $(this).removeClass('rotateToggle').attr("title", "List all credentials");;  
        }else if(!$(this).hasClass('rotateToggle')){
            buildCredentialsList();
            $(this).addClass('rotateToggle').attr("title", "List domain specific credentials");
        }
        if(_savedCredentials.length == 0){
            setNoData();
        }
    });
    
    $('#addBtn').on('click', function(){
        _createEnv = getEnvironment(_domain);
        $('#storeCS .domainName').text(_domain);
        $('#storeCS .envribbon>span').addClass(_createEnv).text(_createEnv);
        $('#credentialsList, #addBtn, #listBtn').hide();
        $('#storeCS').show();
        $('#switchBtn').removeClass('rotateToggle');
    });

    $('#storeBtn').on('click', function(){
        if(validate()){
            saveCredentials();
        }
    });
}

function buildCredentials() {
    _credentialsList = '<span class="appendList">';
        _environment = getEnvironment(_credentialsobj.group);
        _staticEnvironment = _environment;
        _credentialsList += '<div class="domainInfo">\
                                <div class="envribbon">\
                                    <span class="'+_environment+'">'+_environment+'</span>\
                                </div>\
                            <div class="domainName">'+_credentialsobj.domain+'</div>\
                            </div>';
        _.each(_credentialsobj.credentials, function(item) {
            _credentialsList += '<div class="listGroup" data-domainName='+_credentialsobj.domain+'>\
                                    <div class="listName">'+((item.name != undefined)? item.name : item.username)+'</div>\
                                    <div class="listCredentials">\
                                        <i class="btnClass openTabBtn" title="Open in new tab"></i>\
                                            <i class="btnClass removeBtn" title="Delete credentails"></i>\
                                        <div class="username">'+item.username+'</div>\
                                        <div class="password">***********</div>\
                                    </div>\
                                </div>';
        });
    _credentialsList += '</span>';
    
    $('#credentialsList').html(_credentialsList);
    commonEventFunction();
}

function buildCredentialsList() {
    _envList = '';
    _credentialsList = '<span class="appendList">\
                            <div id="envInfo"></div>';
    _listCredentials = _.groupBy(_savedCredentials, 'group');

    _.each(_listCredentials, function(eItem, key) {
        _environment = getEnvironment(key);
        _envList += '<div class="default envTabs '+_environment+'">'+_environment+'</div>';
        _credentialsList += '<div id="'+_environment+'_container" class="envContainer">';
        _.each(eItem, function(dItem) {
            _credentialsList += '<div class="domainInfo">\
                                    <div class="domainName">'+dItem.domain+'</div>\
                                </div>';
            _.each(dItem.credentials, function(cItem) {
                _credentialsList += '<div class="listGroup" data-domainName='+dItem.domain+'>\
                                        <div class="listName">'+((cItem.name != undefined)? cItem.name : cItem.username)+'</div>\
                                <div class="listCredentials">\
                                    <i class="btnClass openTabBtn" title="Open in new tab"></i>\
                                        <i class="btnClass removeBtn" title="Delete credentails"></i>\
                                    <div class="username">'+cItem.username+'</div>\
                                    <div class="password">***********</div>\
                                </div>\
                            </div>';
            });
        });
        _credentialsList += '</div>';
    });
        
    _credentialsList += '</span>';
    
    $('#credentialsList').html(_credentialsList);
    $(_envList).appendTo('#envInfo');
    if(_staticEnvironment != ""){
        $('.envTabs.'+_staticEnvironment+'').removeClass('default');
        $('#'+$('.envTabs.'+_staticEnvironment+'').text()+'_container').show();
    }else{
        $('.envTabs:first-child').removeClass('default');
        $('#'+$('.envTabs:first-child').text()+'_container').show();
    }

    $('.envTabs').on('click', function(){
        $('.envTabs').addClass('default');
        $(this).removeClass('default');
        $('.envContainer').hide();
        $('#'+$(this).text()+'_container').show();
    });
    commonEventFunction();
}

function commonEventFunction(){
    $('.listGroup, .openTabBtn').on('click', function(event){
        event.stopImmediatePropagation();

        var _this;
        if($(this).hasClass('listGroup')){
            _this = $(this);
        }else{
            _this = $(this).parents('.listGroup');
        }

        _domainValue = _this.attr('data-domainName');
        _domainIndex = _.findIndex(_savedCredentials, {domain: _domainValue});
        _usernameValue = _this.find('.username').text();
        _passwordObject = _.findWhere(_savedCredentials[_domainIndex].credentials, {username: _usernameValue});
        
        _messageObject = {
            type: "",
            domain: _domainValue,
            username: _usernameValue,
            password: _passwordObject.password
        }

        if($(this).hasClass('openTabBtn')){
            _messageObject.type = "createTab";
            browser.runtime.sendMessage(_messageObject);
        }else{
            if(_credentialsobj != undefined && !$('#listBtn').hasClass('rotateToggle')) {
                _messageObject.type = "loginTab";
                browser.runtime.sendMessage(_messageObject);
            }else {
                _messageObject.type = "updateTab";
                browser.runtime.sendMessage(_messageObject);
            }
        }
        
        window.close();
    });

    $('.removeBtn').on('click', function(event){
        event.stopImmediatePropagation();

        _domainValue = $(this).parents('.listGroup').attr('data-domainName');
        _domainIndex = _.findIndex(_savedCredentials, {domain: _domainValue});
        if(_savedCredentials[_domainIndex].credentials.length > 1){
            _usernameValue = $(this).parents('.listGroup').find('.username').text();
            _usernameIndex = _.findIndex(_savedCredentials[_domainIndex], {username: _usernameValue});
            _savedCredentials[_domainIndex].credentials.splice(_usernameIndex, 1);
        }else{
            _savedCredentials.splice(_domainIndex, 1);
        }
        browser.storage.sync.set({'data': _savedCredentials}, function(){
            evaluateList();
        });
    });
}

function getEnvironment(_domainName){
    _env = '';

    switch(_domainName) {
        case 'eureqa.dev':
            _env = 'dev';
            break;
        case 'eureqabeta.com':
            _env = 'int';
            break;
        case 'eureqabeta.biz':
            _env = 'biz';
            break;
        case 'eureqatest.xyz':
            _env = 'qa';
            break;
        case 'eureqatest.com':
            _env = 'prod';
            break;
        default:
            _env = 'new';
    }
    return _env;
}

function checkDomain(){
    if(getEnvironment(_domain.substr(_domain.indexOf('.') + 1)) == 'new'){
        $("#addBtn, #listBtn").hide();
        _noDataText = 'Navigate to eureQa web application and click on the<i id="addIcon"></i>button located at the bottom right of the component to start adding credentials.';
    }else{
        _noDataText = 'Click on the<i id="addIcon"></i>button located at the bottom right of the component to start adding credentials.';
    }
}

function evaluateList(){
    checkDomain();
    if(_savedCredentials.length !== 0){
        _credentialsobj = _.findWhere(_savedCredentials, {domain: _domain});
        if(_credentialsobj != undefined){
            buildCredentials();
        }else{
            buildCredentialsList();
            $('#listBtn').addClass('rotateToggle').hide();
        }
    }else{
        setNoData();
    }
}

function getDomainName(){
    browser.tabs.query({currentWindow: true, active: true}, function (tabs){
        url = new URL(tabs[0].url);
        _domain = url.hostname;
        evaluateList();
    });
}

function validate(){
    name = $('#name').val().trim();
    username = $('#username').val().trim();
    password = $('#password').val().trim();

    storePayload = '';

    validateState = false;

    if(username.length == 0){
        $('#username').focus();
    }else if(password.length == 0){
        $('#password').focus();
    }else {
        storePayload = {
            domain: _domain,
            group: _domain.substr(_domain.indexOf('.') + 1),
            credentials: [{
                'username': username,
                'password': btoa(password)
            }] 
        }
        if(name.length != 0){
            storePayload.credentials[0].name = name;
        }
        validateState = true;
    }
    return validateState;
}

function setNoData(){
    $('#listBtn').hide();
    if(getEnvironment(_domain.substr(_domain.indexOf('.') + 1)) == 'new'){
        $("#addBtn").hide();
    }
    $('#credentialsList').html('<div id="noData"><div id="emptyStore">Empty Store!</div><div>'+ _noDataText +'</div></div>');
}

$("#name, #username, #password").bind("keyup",function(event){
	if ( event.keyCode == 13 ) {
		$("#storeBtn").click();
	 }
});

function saveCredentials(){
    if(validate()){
        _findIndex = _.findIndex(_savedCredentials, {domain: _domain});

        if(_findIndex != -1){
            _savedCredentials[_findIndex].credentials.push(storePayload.credentials[0]);
        }else{
            if(_.findWhere(_savedCredentials, {domain: 'sampledomain.com'})!=undefined){
                _savedCredentials.pop()
            }
            _savedCredentials.push(storePayload);
        }
        
        browser.storage.sync.set({'data' : _savedCredentials}, function(){
            $('#storeCS input').val('');
        });
        
        evaluateList();
        $('#switchBtn').trigger('click');
    }
}