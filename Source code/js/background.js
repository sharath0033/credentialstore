var appTabId = -1;
var appUsername = "";
var appPassword = "";

function tabsOnUpdatedHandler(tabId, changeInfo, tabInfo){
    if (appTabId !== -1 && tabId == appTabId && changeInfo.status == "complete") {
        sendLoginInfo("waitLogin", tabId, appUsername, appPassword);
    }
}

function sendLoginInfo(_action, _tabId, _username, _password){
    browser.tabs.sendMessage(_tabId, {type: _action, username: _username, password: _password}, function(){
        appTabId = -1;
    });
}

browser.runtime.onMessage.addListener(
    function(response,sender,sendResponse) {
    if(response.type !== undefined){
        switch(response.type) {
            case 'loginTab':
                browser.tabs.query({currentWindow: true, active: true}, function (tabs){
                    sendLoginInfo("login", tabs[0].id, response.username, response.password);
                });
                break;
            case 'updateTab':
                browser.tabs.update({active: true, url: 'https://'+response.domain}, function(tab){
                    appTabId = tab.id;
                    appUsername = response.username;
                    appPassword = response.password;
                });
                break;
            case 'createTab':
                browser.tabs.create({active: true, url: 'https://'+response.domain}, function(tab){
                    appTabId = tab.id;
                    appUsername = response.username;
                    appPassword = response.password;
                });
                break;
        }
    }
});

browser.tabs.onUpdated.addListener(tabsOnUpdatedHandler);