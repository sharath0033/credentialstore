function login(_username, _password) {
    document.getElementById("loginForm:userId").value = _username;
    document.getElementById("loginForm:password").value = atob(_password);
    document.getElementById("loginForm:loginButton").click();
}

browser.runtime.onMessage.addListener(
    function(response,sender,sendResponse) {
    if(response.type == "login"){
        login(response.username, response.password);
    }else if(response.type == "waitLogin"){
        var count = 0;
        var interval = setInterval(function() {
            if (count > (30000/500)) {
                clearInterval(interval);
            }
            if (document.getElementById("loginForm:userId") == null) {
                count++;
            } else {
                clearInterval(interval);
                login(response.username, response.password);
            }
        }, 500);
    }
});